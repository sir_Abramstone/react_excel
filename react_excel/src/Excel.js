import React, {Component} from 'react';
import './App.css';
import PropTypes from 'prop-types';

class Excel extends Component {
    constructor(props) {
        super(props);
        this._sort = this._sort.bind(this);
        this._showEditor = this._showEditor.bind(this);
        this._save = this._save.bind(this);
        this._renderTable = this._renderTable.bind(this);
        this._renderToolbar = this._renderToolbar.bind(this);
        this._renderSearch = this._renderSearch.bind(this);
        this._toggleSearch = this._toggleSearch.bind(this);
        this._search = this._search.bind(this);
        this._logSetState = this._logSetState.bind(this);
        this._replay = this._replay.bind(this);
        this._download = this._download.bind(this);
        this.state = {
            data: props.initialData,
            sortBy: null,
            descending: false,
            edit: null, // [row index, cell index]
            search: false,
        };
    }

    _log = [];

    _logSetState(newState) {
        this._log.push(JSON.parse(JSON.stringify(
            this._log.length === 0 ? this.state : newState
        )));
        this.setState(newState);
    }

    componentDidMount() {
        document.onkeydown = function (e) {
            if (e.altKey && e.shiftKey && e.keyCode === 82) {
                this._replay();
            }
        }.bind(this);
    }

    _replay() {
        if (this._log.length === 0) {
            console.warn("нет событий");
            return;
        }
        let idx = -1;
        let interval = setInterval(function () {
            idx++;
            if (idx === this._log.length - 1) {
                clearInterval(interval);
            }
            this.setState(this._log[idx]);
        }.bind(this), 1000);
    }

    _preSearchData = null;

    _sort(e) {
        let column = e.target.cellIndex;
        let data = Array.from(this.state.data);
        let descending = this.state.sortBy === column && !this.state.descending;
        data.sort(function (a, b) {
            return descending
                ? (a[column] < b[column] ? 1 : -1)
                : (a[column] > b[column] ? 1 : -1);
        });
        this._logSetState({
            data: data,
            sortBy: column,
            descending: descending,
        });
    }

    _showEditor(e) {
        this._logSetState({
            edit: {
                row: parseInt(e.target.dataset.row, 10),
                cell: e.target.cellIndex,
            }
        });
    }

    _save(e) {
        e.preventDefault();
        let input = e.target.firstChild;
        let data = Array.from(this.state.data);
        data[this.state.edit.row][this.state.edit.cell] = input.value;
        this._logSetState({
            edit: null,
            data: data,
        });
    }

    _download(format, ev) {
        let contents = format === 'json'
            ? JSON.stringify(this.state.data)
            : this.state.data.reduce(function (result, row) {
                return result + row.reduce(function (rowresult, cell, idx) {
                        return rowresult
                            + '""'
                            + cell.replace(/"/g, '""')
                            + '"'
                            + (idx < row.length - 1 ? ',' : '');
                    }, '')
                    + "\n";
            }, '');
        let URL = window.URL || window.webkitURL;
        let blob = new Blob([contents], {type: 'text/' + format});
        ev.target.href = URL.createObjectURL(blob);
        ev.target.download = 'data.' + format;
    }

    _search(e) {
        let needle = e.target.value.toLowerCase();
        if (!needle) {
            this._logSetState({
                data: this._preSearchData,
            });
            return;
        }
        let idx = e.target.dataset.idx;
        let searchdata = this._preSearchData.filter(function (row) {
            return row[idx].toString().toLowerCase().indexOf(needle) > -1;
        });
        this._logSetState({
            data: searchdata,
        });
    }

    _toggleSearch() {
        if (this.state.search) {
            this._logSetState({
                data: this._preSearchData,
                search: false,
            });
            this._preSearchData = null;
        } else {
            this._preSearchData = this.state.data;
            this._logSetState({
                search: true,
            });
        }
    }

    _renderSearch() {
        if (!this.state.search) {
            return null;
        }
        return (
            <tr onChange={this._search}>
                {this.props.headers.map(function (_ignore, idx) {
                    return <td key={idx}><input type="text" data-idx={idx}/></td>
                })
                }</tr>
        );
    }

    _renderToolbar() {
        return <div className="toolbar">
            <button onClick={this._toggleSearch}>search</button>
            <a onClick={this._download.bind(this, 'json')} href="data.json">Export JSON</a>
            <a onClick={this._download.bind(this, 'csv')} href="data.csv">Export CSV</a>
        </div>
    }

    _renderTable() {
        return (
            <table>
                <thead onClick={this._sort}>
                <tr>{
                    this.props.headers.map(function (title, idx) {
                        if (this.state.sortBy === idx) {
                            title += this.state.descending ? ' \u2191' : ' \u2193'
                        }
                        return <th key={idx}>{title}</th>
                    }, this)
                }
                </tr>
                </thead>
                <tbody onDoubleClick={this._showEditor}>
                {this._renderSearch()}
                {this.state.data.map(function (row, rowidx) {
                    return (
                        <tr key={rowidx}>{
                            row.map(function (cell, idx) {
                                let content = cell;
                                let edit = this.state.edit;
                                if (edit && edit.row === rowidx && edit.cell === idx) {
                                    content =
                                        <form onSubmit={this._save}><input type='text' defaultValue={content}></input>
                                        </form>
                                }
                                return <td key={idx} data-row={rowidx}>{content}</td>;
                            }, this)}
                        </tr>
                    );
                }, this)}
                </tbody>
            </table>
        );
    }

    render() {
        return (
            <div>
                {this._renderToolbar()}
                {this._renderTable()},
            </div>
        )
    }
}


Excel.propTypes = {
    headers: PropTypes.arrayOf(
        PropTypes.string
    ),
    initialData: PropTypes.arrayOf(
        PropTypes.arrayOf(
            PropTypes.string
        )
    ),
};

export default Excel;
